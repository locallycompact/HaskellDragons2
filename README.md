# Haskell Dragons 2

A example repository introducing YAML, Maybe and Docopt.

This is part of the web book 'A Nonlinear Guide to Haskell', found
[here](http://locallycompact.gitlab.io/ANLGTH/)
