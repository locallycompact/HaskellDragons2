{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes #-}

module Main where

import Control.Applicative
import Control.Monad (when)
import qualified Data.ByteString.Char8 as BS
import Data.Yaml
import GHC.Generics
import System.Environment (getArgs)
import System.Console.Docopt

data Color = Red | Green | Blue deriving (Show, Generic, FromJSON, ToJSON)

data Dragon = Dragon {
  name :: String,
  heads :: Int,
  color :: Color
} deriving (Show, Generic, FromJSON, ToJSON)

fight :: Dragon -> Dragon -> Dragon
fight x y = if heads x > heads y then x else y

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

main :: IO ()
main = do
  args <- parseArgsOrExit patterns =<< getArgs

  when (args `isPresent` (command "fight")) $ do
    f1 <- BS.readFile =<< args `getArgOrExit` (argument "d1")
    f2 <- BS.readFile =<< args `getArgOrExit` (argument "d2")
    let d1 = decode f1 :: Maybe Dragon
    let d2 = decode f2 :: Maybe Dragon
    print d1
    print d2
    let winner = liftA2 fight d1 d2
    print winner
